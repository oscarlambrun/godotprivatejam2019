using Godot;
using System;

public class BulletRigidBody : RigidBody2D
{
    public override void _Ready()
    {
        Connect("body_entered", this, nameof(OnCollision));
    }

    public void OnCollision(Node node)
    {
        this.QueueFree();
    }
}
