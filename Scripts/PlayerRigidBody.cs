using Godot;
using System;
using System.Collections.Generic;

public class PlayerRigidBody : RigidBody2D
{
    private const int _impulseFactor = 30;
    private Node2D _mapScene;
    private Gun _gun;

    public override void _Ready()
    {
        _mapScene = GetParent<Node2D>();
        _gun = new Gun(_mapScene, this);
        AddChild(_gun);
        Friction = 0;
    }

    public override void _PhysicsProcess(float delta)
    {
        InputPlayerMoves();
        InputPlayerShoots();
    }

    public override void _IntegrateForces(Physics2DDirectBodyState state)
    {
        LookFollow(state, GetGlobalTransform(), GetGlobalMousePosition());
    }

    private void InputPlayerMoves()
    {
        var slowDownFactor = Input.IsActionPressed("ui_shift") ? 0.2f : 1;
        Vector2 impulseVector = new Vector2(
            Input.IsActionPressed("ui_left") ? -1 : Input.IsActionPressed("ui_right") ? 1 : 0,
            Input.IsActionPressed("ui_up") ? -1 : Input.IsActionPressed("ui_down") ? 1 : 0).Normalized();

        ApplyImpulse(Vector2.Zero, impulseVector * _impulseFactor * slowDownFactor);
    }

    private void InputPlayerShoots()
    {
        if (Input.IsActionPressed("ui_left_click"))
            _gun.TriggerPulled();
        else if (Input.IsActionJustReleased("ui_left_click"))
            _gun.TriggerReleased();
    }
   
    private void LookFollow(Physics2DDirectBodyState state, Transform2D currentTransform, Vector2 targetPosition)
    {
        var targetDir = (targetPosition - currentTransform.origin).Normalized();
        this.SetRotation(Mathf.Atan2(targetDir.y, targetDir.x) + Mathf.Pi/2);
    }
}
