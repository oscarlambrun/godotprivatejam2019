using Godot;
using System;

public class Gun : Node
{
    private Node2D _mapScene;
    private Node2D _owner;
    private PackedScene _bulletPackedScene;
    private bool _isTriggerPulled;
    private int _velocity;
    private AudioStreamPlayer2D _shotAudio;

    public Gun()
    {
    }

    public Gun(Node2D mapScene, Node2D owner)
    {
        _mapScene = mapScene;
        _owner = owner;
        _bulletPackedScene = GD.Load<PackedScene>("res://Scenes/Bullet.tscn");
        LoadShotSound();
        _velocity = 1500;

    }

    private void LoadShotSound()
    {
        _shotAudio = new AudioStreamPlayer2D();
        AddChild(_shotAudio);
        _shotAudio.Stream = GD.Load<AudioStream>("res://Sounds/GunSilencer.wav");
    }

    public void TriggerPulled()
    {
        if (_isTriggerPulled)
            return;

        _isTriggerPulled = true;
        var bulletRigidBody = (RigidBody2D)_bulletPackedScene.Instance();
        _mapScene.AddChild(bulletRigidBody);
        SetBulletProperties(bulletRigidBody);
        _shotAudio.Play();
    }

    public void TriggerReleased()
    {
        _isTriggerPulled = false;
    }

    private void SetBulletProperties(RigidBody2D bulletRigidBody)
    {
        var mouseToVector = GetPlayerToMouseVector();
        bulletRigidBody.Position = _owner.Position + (mouseToVector * 45);
        bulletRigidBody.ApplyImpulse(Vector2.Zero, mouseToVector * _velocity);
    }

    public Vector2 GetPlayerToMouseVector()
    {
        return (_mapScene.GetGlobalMousePosition() - _owner.GetGlobalPosition()).Normalized();
    }
}
