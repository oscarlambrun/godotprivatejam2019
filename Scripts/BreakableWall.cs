using Godot;
using System;

public class BreakableWall : StaticBody2D
{
    public override void _Ready()
    {
        Connect("body_entered", this, nameof(OnCollision));
    }

    public void OnCollision(Node node)
    {
        GD.Print("ouch");
    }
}
