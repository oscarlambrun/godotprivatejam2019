using Godot;
using System;

public class Peon : Node2D
{
    private Viewport _root;
    private Lazy<Navigation2D> _navigation;
    private Lazy<RigidBody2D> _player;
    private Lazy<Line2D> _line;

    public override void _Ready()
    {
        _root = this.GetTree().GetRoot();
        _navigation = new Lazy<Navigation2D>(() => (Navigation2D)_root.FindNode("Navigation2D", true, false));
        _player = new Lazy<RigidBody2D>(() => (RigidBody2D)_root.FindNode("Player", true, false));
        _line = new Lazy<Line2D>(() => (Line2D)_root.FindNode("Line2D", true, false));
    }

    public override void _Process(float delta)
    {
        var test = _navigation.Value.GetSimplePath(this.Position, _player.Value.Position);
        _line.Value.SetPoints(test);
    }

    public override void _Input(InputEvent inputevent)
    {
    }
}
